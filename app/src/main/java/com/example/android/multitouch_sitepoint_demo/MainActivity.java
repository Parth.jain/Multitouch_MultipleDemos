package com.example.android.multitouch_sitepoint_demo;

import android.content.Intent;
import android.opengl.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity{

    private GestureDetector mGestureDetector;
    Button mBtnGotoScale,mBtnGotoDrag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Android_Gesture_Detector android_gesture_detector=new Android_Gesture_Detector();
        mGestureDetector=new GestureDetector(this,android_gesture_detector);


        mBtnGotoScale=findViewById(R.id.btnScaleAct);
        mBtnGotoDrag=findViewById(R.id.btnDragAct);
        mBtnGotoScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ScaleActivity.class));
            }
        });

        mBtnGotoDrag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,DragActivity.class));
            }
        });



    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
}

