package com.example.android.multitouch_sitepoint_demo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.provider.DocumentsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class DragActivity extends AppCompatActivity implements View.OnTouchListener {

    int clickCount;
    private ViewGroup RootLayout;
    private int Position_X;
    private int Position_Y;

    long startTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag);
        RootLayout=findViewById(R.id.rootlayout);

        Button mBtnAddImage=findViewById(R.id.addImageBtn);

        mBtnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Add_Image();

            }
        });
        clickCount=0;

    }
    private void Add_Image() {
    final ImageView iv=new ImageView(this);
    iv.setImageResource(R.drawable.image);

        RelativeLayout.LayoutParams layoutParams=new RelativeLayout.LayoutParams(500,500);
        iv.setLayoutParams(layoutParams);
        RootLayout.addView(iv);
        iv.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(final View view, MotionEvent event) {
            int X= (int) event.getRawX();
            int Y= (int) event.getRawY();

            int pointerCount=event.getPointerCount();

            switch (event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    Position_X = X - layoutParams.leftMargin;
                    Position_Y = Y - layoutParams.topMargin;
                    break;

                case MotionEvent.ACTION_UP:
                    if (startTime == 0){

                        startTime = System.currentTimeMillis();

                    }else {
                        if (System.currentTimeMillis() - startTime < 200) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(DragActivity.this);
                            builder.setMessage("Are you sure you want to delete this?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    view.setVisibility(View.GONE);

                                }
                            });

                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                        }

                        startTime = System.currentTimeMillis();

                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    break;

                case MotionEvent.ACTION_MOVE:

                    if (pointerCount == 1){
                        RelativeLayout.LayoutParams Params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        Params.leftMargin = X - Position_X;
                        Params.topMargin = Y - Position_Y;
                        Params.rightMargin = -500;
                        Params.bottomMargin = -500;
                        view.setLayoutParams(Params);
                    }

                    if (pointerCount == 2){

                        RelativeLayout.LayoutParams layoutParams1 =  (RelativeLayout.LayoutParams) view.getLayoutParams();
                        layoutParams1.width = Position_X +(int)event.getX();
                        layoutParams1.height = Position_Y + (int)event.getY();
                        view.setLayoutParams(layoutParams1);
                    }
                    //Rotation
                    if (pointerCount == 3){
                        //Rotate the ImageView
                        view.setRotation(view.getRotation() + 10.0f);
                    }
                    break;
            }
        RootLayout.invalidate();
        return true;
    }
}
